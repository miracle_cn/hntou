package com.huanghun.hospital;

import com.huanghun.hospital.mapper.HospitalSetMapper;
import com.huanghun.hospital.model.HospitalSet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HospitalManageApplicationTests {

    @Autowired
    private HospitalSetMapper hospitalSetMapper;


    @Test
    void contextLoads() {
        HospitalSet hospitalSet = hospitalSetMapper.selectById("1");
        String signKey = hospitalSet.getSignKey();
        System.out.println(signKey);
    }

}
