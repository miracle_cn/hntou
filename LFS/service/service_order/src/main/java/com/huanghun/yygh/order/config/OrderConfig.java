package com.huanghun.yygh.order.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.huanghun.yygh.order.mapper")
public class OrderConfig {
}
