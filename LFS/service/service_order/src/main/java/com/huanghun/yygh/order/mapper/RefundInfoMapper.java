package com.huanghun.yygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.order.RefundInfo;

public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}

