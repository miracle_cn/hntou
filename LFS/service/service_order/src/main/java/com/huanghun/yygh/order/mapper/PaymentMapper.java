package com.huanghun.yygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.order.PaymentInfo;

public interface PaymentMapper extends BaseMapper<PaymentInfo> {
}
