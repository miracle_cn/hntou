package com.huanghun.yygh.user.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(basePackages = "com.huanghun.yygh.user.mapper")
public class UserConfig {
}
