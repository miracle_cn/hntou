package com.huanghun.yygh.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.huanghun")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.huanghun")

public class UserS {
    public static void main(String[] args) {
        SpringApplication.run(UserS.class,args);
    }
}
