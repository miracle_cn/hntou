package com.huanghun.yygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.user.Patient;

public interface PatientMapper extends BaseMapper<Patient> {
}
