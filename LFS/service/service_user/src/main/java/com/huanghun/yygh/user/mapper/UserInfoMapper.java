package com.huanghun.yygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.user.UserInfo;

public interface UserInfoMapper  extends BaseMapper<UserInfo> {
}
