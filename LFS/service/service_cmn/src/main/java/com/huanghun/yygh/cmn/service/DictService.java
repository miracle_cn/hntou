package com.huanghun.yygh.cmn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanghun.yygh.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface DictService extends IService<Dict> {
    List<Dict> findChlidData(Long id);

    public void exportDictData(HttpServletResponse response);

    public void importDictData(MultipartFile file);

    String getDictName(String dictCode, String value);
    //根据dictCode获取下级节点
    List<Dict> findByDictCode(String dictCode);
}
