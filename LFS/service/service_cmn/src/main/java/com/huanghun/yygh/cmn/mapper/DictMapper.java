package com.huanghun.yygh.cmn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.cmn.Dict;


public interface DictMapper extends BaseMapper<Dict> {
}
