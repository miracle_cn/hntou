package com.huanghun.yygh.msm.service;

import com.huanghun.yygh.vo.msm.MsmVo;

public interface SendEmailService {
    //发送短信
    boolean send(String phone, String code);
    //用于MQ发送短信
    boolean send(MsmVo msmVo);
}
