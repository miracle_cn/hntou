package com.huanghun.yygh.msm.service.imlp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.huanghun.yygh.msm.service.SendEmailService;
import com.huanghun.yygh.vo.msm.MsmVo;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

@Service
public class SendEmailServiceImpl implements SendEmailService {



    @Override
    public boolean send(String phone, String code) {

        try {
            HtmlEmail email = new HtmlEmail();
            //用哪家服务器的就.什么就行，这里用的QQ邮箱
            email.setHostName("smtp.qq.com");
            //编码方式
            email.setCharset("utf-8");
            //发送的目的地
            email.addTo(phone);
            //发件人
            email.setFrom("1391304884@qq.com", "1100110");
            //发件人，以及其对应的授权码
            email.setAuthentication(phone, "thpquwnbalynhjhg");
            //发送主体
            email.setSubject("邮箱验证码");
            //发送内容
            email.setMsg(code);
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean send(MsmVo msmVo) {
        if(!StringUtils.isEmpty(msmVo.getPhone())){
            boolean isSend = this.send(msmVo.getPhone(), msmVo.getParam());
            return isSend;
        }
        return false;
    }



    private boolean send(String phone, Map<String,Object> param) {

        try {
            HtmlEmail email = new HtmlEmail();
            //用哪家服务器的就.什么就行，这里用的QQ邮箱
            email.setHostName("smtp.qq.com");
            //编码方式
            email.setCharset("utf-8");
            //发送的目的地
            email.addTo(phone);
            //发件人
            email.setFrom("1391304884@qq.com", "1100110");
            //发件人，以及其对应的授权码
            email.setAuthentication(phone, "thpquwnbalynhjhg");
            //发送主体
            email.setSubject("邮箱验证码");
            //发送内容
            email.setMsg(JSONObject.toJSONString(param));
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


}
