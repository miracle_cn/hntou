package com.huanghun.yygh.msm.controller;

import com.huanghun.yygh.common.result.Result;
import com.huanghun.yygh.msm.utils.RandomUtil;
import com.huanghun.yygh.msm.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/msm")
public class SendEmailController {

    @Autowired
    private SendEmailService sendEmailService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //发送手机验证码
    @GetMapping("send/{phone}")
    public Result sendCode(@PathVariable String phone) {
        //从redis获取验证码，如果获取获取到，返回ok
        // key 手机号  value 验证码
        String code = redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(code)) {
            return Result.ok();
        }
        //如果从redis获取不到，
        // 生成六位验证码
        code = RandomUtil.getSixBitRandom();
        System.out.println(phone + " " + code);
        redisTemplate.opsForValue().set(phone, code, 2, TimeUnit.MINUTES);

        //调用service方法，通过整合邮箱服务进行发送
        boolean isSend = sendEmailService.send(phone, code);
        //生成验证码放到redis里面，设置有效时间
        if (isSend) {
            //两分钟之内有效
            redisTemplate.opsForValue().set(phone, code, 2, TimeUnit.MINUTES);
            System.out.println("发送成功");
            return Result.ok();
        } else {
            return Result.fail().message("发送邮件失败");
        }
    }
}
