package com.huanghun.yygh.hosp.service;

import com.huanghun.yygh.vo.hosp.DepartmentQueryVo;
import com.huanghun.yygh.vo.hosp.DepartmentVo;
import com.huanghun.yygh.model.hosp.Department;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DepartmentService {
    void save(Map<String, Object> paramMap);
    //查询医院科室
    Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo);

    void remove(String hospcode, String depcode);

    List<DepartmentVo> findDeptTree(String hoscode);
    //根据医院编号 科室编号 查询科室名称
    Object getDepName(String hoscode, String depcode);

    Department getDepartment(String hoscode, String depcode);
}
