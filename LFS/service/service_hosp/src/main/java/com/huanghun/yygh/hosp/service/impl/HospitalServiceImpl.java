package com.huanghun.yygh.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.huanghun.yygh.cmn.client.DictFeignClient;
import com.huanghun.yygh.vo.hosp.HospitalQueryVo;
import com.huanghun.yygh.hosp.repository.HospitalRepository;
import com.huanghun.yygh.hosp.service.HospitalService;
import com.huanghun.yygh.model.hosp.Hospital;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HospitalServiceImpl implements HospitalService {

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private DictFeignClient dictFeignClient;

    @Override
    public void save(Map<String, Object> objectMap) {
        //把参数map集合转换成对象Hospital
        String mapString = JSONObject.toJSONString(objectMap);
        Hospital hospital = JSONObject.parseObject(mapString, Hospital.class);

        //判断是否存在数据
        String hoscode = hospital.getHoscode();
        Hospital hospitalExist = hospitalRepository.getHospitalByHoscode(hoscode);

        //如果存在，进行修改
        if(hospitalExist != null){
            hospital.setStatus(hospitalExist.getStatus());
            hospital.setCreateTime(hospitalExist.getCreateTime());
            hospital.setUpdateTime((new Date()));
            hospital.setIsDeleted(0);
        }else{//如果不存在，进行添加
            hospital.setStatus(0);
            hospital.setCreateTime(new Date());
            hospital.setUpdateTime(new Date());
            hospital.setIsDeleted(0);
            hospitalRepository.save(hospital);
        }
    }

    @Override
    public Hospital getByHoscode(String hospcode) {
        Hospital hospital = hospitalRepository.getHospitalByHoscode(hospcode);
        return hospital;
    }

    //医院列表，条件查询分页
    @Override
    public Page<Hospital> selectHospPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo) {
        //创建pageable对象
        PageRequest pageable = PageRequest.of(page - 1, limit);
        //创建条件匹配器
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase(true);

        Hospital hospital = new Hospital();
        BeanUtils.copyProperties(hospitalQueryVo,hospital);

        //创建对象
        Example<Hospital> example = Example.of(hospital, matcher);
        //调用方法实现查询
        Page<Hospital> pages = hospitalRepository.findAll(example, pageable);

        List<Hospital> content = pages.getContent();

        //获取查询的list集合，遍历进行医院等级封装
        pages.getContent().stream().forEach(item ->{
            this.setHospitalHospType(item);
        });
        return pages;
    }
    //根据id更新上线状态
    @Override
    public void updateStatus(String id, Integer status) {
        //根据id查询要修改的医院对象
        Hospital hospital = hospitalRepository.findById(id).get();
        //修改状态
        hospital.setStatus(status);
        hospital.setUpdateTime(new Date());
        //保存到
        hospitalRepository.save(hospital);
    }

    @Override
    public Map<String, Object> getHospById(String id) {
        HashMap<String, Object> result = new HashMap<>();
        Hospital hospital = this.setHospitalHospType(hospitalRepository.findById(id).get());
        //医院信息，包含医院等级信息
        result.put("hospital",hospital);
        //单独处理更直观
        result.put("bookingRule", hospital.getBookingRule());
        //不需要重复返回
        hospital.setBookingRule(null);
        return result;
    }


    //获取医院名称
    @Override
    public String getHospName(String hoscode) {
        Hospital hospital = hospitalRepository.getHospitalByHoscode(hoscode);
        if(hospital != null){
            return hospital.getHosname();
        }
        return null;
    }

    //根据医院名称查询
    @Override
    public List<Hospital> findByHosName(String hosname) {
        return hospitalRepository.findHositalByHosnameLike(hosname);
    }

    @Override
    public Map<String, Object> item(String hoscode) {
        Map<String, Object> result = new HashMap<>();
        //医院详情
        Hospital hospital = this.setHospitalHospType(this.getByHoscode(hoscode));
        result.put("hospital", hospital);
        //预约规则
        result.put("bookingRule", hospital.getBookingRule());
        //不需要重复返回
        hospital.setBookingRule(null);
        return result;

    }


    private Hospital setHospitalHospType(Hospital hospital) {
        //根据dictCode和value获取医院等级名称
        String hostype = dictFeignClient.getName("Hostype", hospital.getHostype());

        //查询省 市 地区
        String province = dictFeignClient.getName(hospital.getProvinceCode());
        String city = dictFeignClient.getName(hospital.getCityCode());
        String district = dictFeignClient.getName(hospital.getDistrictCode());

        hospital.getParam().put("fullAddress",province + city + district);
        hospital.getParam().put("hosptypeString",hostype);

        return hospital;
    }
}
