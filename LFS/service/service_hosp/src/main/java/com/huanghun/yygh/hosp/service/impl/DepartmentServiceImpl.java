package com.huanghun.yygh.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.huanghun.yygh.hosp.repository.DepartmentRepository;
import com.huanghun.yygh.hosp.service.DepartmentService;
import com.huanghun.yygh.vo.hosp.DepartmentQueryVo;
import com.huanghun.yygh.vo.hosp.DepartmentVo;
import com.huanghun.yygh.model.hosp.Department;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;

    //上传医院科室
    @Override
    public void save(Map<String, Object> paramMap) {
        String paramMapString = JSONObject.toJSONString(paramMap);
        Department department = JSONObject.parseObject(paramMapString, Department.class);

        //根据医院编号和科室编号查询
        Department departmentExist = departmentRepository.
                getDepartmentByHoscodeAndDepcode(department.getHoscode(),department.getDepcode());

        if(departmentExist != null){
            departmentExist.setUpdateTime(new Date());
            departmentExist.setIsDeleted(0);
            departmentRepository.save(departmentExist);
        }else{
            department.setUpdateTime(new Date());
            department.setCreateTime(new Date());
            department.setIsDeleted(0);
            departmentRepository.save(department);
        }


    }

    @Override
    public Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo) {
        //创建Pageable对象，设置当前页和每页记录数
        //0是第一页
        PageRequest pageable = PageRequest.of(page - 1, limit);
        //创建Example对象
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase(true);

        Department department = new Department();
        BeanUtils.copyProperties(departmentQueryVo,department);
        department.setIsDeleted(0);

        Example<Department> example = Example.of(department,matcher);
        Page<Department> all = departmentRepository.findAll(example, pageable);
        return all;
    }


    //删除医院科室
    @Override
    public void remove(String hospcode, String depcode) {
        Department department = departmentRepository.getDepartmentByHoscodeAndDepcode(hospcode, depcode);
        if(department != null){
            //调用方法删除
            departmentRepository.deleteById(department.getId());
        }
    }
    //根据医院编号，查询医院所有的科室信息
    @Override
    public List<DepartmentVo> findDeptTree(String hoscode) {
        ArrayList<DepartmentVo> result = new ArrayList<>();

        //根据医院编号，查询医院所有的科室信息
        Department department = new Department();
        department.setHoscode(hoscode);
        Example  example = Example.of(department);
        //所有科室列表
        List<Department> all = departmentRepository.findAll(example);

        //根据大科室编号 bigcode分组，获取每个大科室里面的下级子科室
        Map<String, List<Department>> departmentMap =
                all.stream().collect(Collectors.groupingBy(Department::getBigcode));

        //遍历map集合 departmentMap
        for(Map.Entry<String,List<Department>> entry : departmentMap.entrySet()){
            //大科室编号
            String bigcode = entry.getKey();

            //大科室编号对应的全局数据
            List<Department> departments = entry.getValue();

            //封装大科室

            DepartmentVo departmentVo1 = new DepartmentVo();
            departmentVo1.setDepcode(bigcode);
            departmentVo1.setDepname(departments.get(0).getBigname());


            //封装小科室
            ArrayList<DepartmentVo> children = new ArrayList<>();
            for(Department each : departments){
                DepartmentVo departmentVo2 = new DepartmentVo();
                departmentVo2.setDepcode(each.getDepcode());
                departmentVo2.setDepname(each.getDepname());
                children.add(departmentVo2);
            }

            //把小科室对象的集合放到大科室对象里面去
            departmentVo1.setChildren(children);
            //再将大科室对象放到集合中
            result.add(departmentVo1);
        }
        return  result;
    }

    //根据科室编号，和医院编号，查询科室名称
    @Override
    public String getDepName(String hoscode, String depcode) {
        Department department = departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode, depcode);
        if(department != null) {
            return department.getDepname();
        }
        return null;
    }

    @Override
    public Department getDepartment(String hoscode, String depcode) {
        return departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode, depcode);
    }

}
