package com.huanghun.yygh.hosp.service;

import com.huanghun.yygh.vo.hosp.HospitalQueryVo;
import com.huanghun.yygh.model.hosp.Hospital;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface HospitalService {
    //上传医院接口
    void save(Map<String, Object> objectMap);
    //根据医院编号进行查询
    Hospital getByHoscode(String hospcode);
    //分页查询
    Page<Hospital> selectHospPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo);
    //根据id更新上线状态
    void updateStatus(String id, Integer status);

    //根据id获取详情信息
    Map<String, Object> getHospById(String id);

    //获取医院名称
    String getHospName(String hoscode);

    List<Hospital> findByHosName(String hosname);

    Map<String, Object> item(String hoscode);
}
