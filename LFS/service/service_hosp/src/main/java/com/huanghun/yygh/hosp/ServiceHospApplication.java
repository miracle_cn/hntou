package com.huanghun.yygh.hosp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.huanghun")//扫描这个包下面的bean
@EnableDiscoveryClient //加上注解后能够在Nacos注册中心注册服务
@EnableFeignClients(basePackages = "com.huanghun")//扫描这个包下面的bean
public class ServiceHospApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospApplication.class,args);
    }
}
