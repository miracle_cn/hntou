package com.huanghun.yygh.hosp.controller.api;

import com.huanghun.yygh.common.exception.YyghException;
import com.huanghun.yygh.common.result.Result;
import com.huanghun.yygh.common.result.ResultCodeEnum;
import com.huanghun.yygh.vo.hosp.DepartmentQueryVo;
import com.huanghun.yygh.vo.hosp.ScheduleQueryVo;
import com.huanghun.yygh.helper.HttpRequestHelper;
import com.huanghun.yygh.hosp.service.DepartmentService;
import com.huanghun.yygh.hosp.service.HospitalService;
import com.huanghun.yygh.hosp.service.HospitalSetService;
import com.huanghun.yygh.hosp.service.ScheduleService;
import com.huanghun.yygh.model.hosp.Department;
import com.huanghun.yygh.model.hosp.Hospital;
import com.huanghun.yygh.model.hosp.Schedule;
import com.huanghun.yygh.util.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/hosp")
public class ApiController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;


    //删除排班
    @PostMapping("schedule/remove")
    public Result remove(HttpServletRequest request){
        //获取传过来的医院排班信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取医院编号和排班编号
        String hoscode = (String) paramMap.get("hoscode");
        String hosScheduleId = (String) paramMap.get("hosScheduleId");

        //TODO 签名校验


        scheduleService.remove(hoscode,hosScheduleId);

        return Result.ok();

    }



    //查询排班
    @PostMapping("schedule/list")
    public Result findSchedule (HttpServletRequest request){
        //获取传过来的医院排班信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");

        //科室编号
        String depcode = (String) paramMap.get("depcode");

        //当前页号
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String)paramMap.get("page"));
        //每页记录数
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));

        //TODO 签名校验


        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);

        //调用service接口
        Page<Schedule> pages = scheduleService.findPageSchedule(page,limit,scheduleQueryVo);

        return Result.ok(pages);
    }


    //上传排班
    @PostMapping("saveSchedule")
    public Result saveScheduleService(HttpServletRequest request){
        //获取传过来的医院排班信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        scheduleService.save(paramMap);
        return Result.ok();
    }


    //删除科室
    @PostMapping("department/remove")
    public Result removeDepartment(HttpServletRequest request){
        //获取传过来的医院科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        String hoscode = (String) paramMap.get("hoscode");
        String depcode = (String) paramMap.get("depcode");

        //TODO 签名校验



        departmentService.remove(hoscode,depcode);
        return Result.ok();
    }


    //查询医院科室
    @PostMapping("department/list")
    public Result findDepartment(HttpServletRequest request){
        //获取传过来的医院科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");

        //当前页号
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String)paramMap.get("page"));
        //每页记录数
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));


        //TODO 签名校验

        DepartmentQueryVo departmentQueryVo = new DepartmentQueryVo();
        departmentQueryVo.setDepcode(hoscode);

        //调用service接口
        Page<Department> pages = departmentService.findPageDepartment(page,limit,departmentQueryVo);

        return Result.ok(pages);
    }


    //上传科室
    @PostMapping("saveDepartment")
    public Result saveDepartment(HttpServletRequest request){
        //获取传过来的医院科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSign = (String)paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String hoscode = (String)paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);


        //3.把数据库中查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hospSign.equals(signKeyMd5)){
            throw  new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //调用service方法
        departmentService.save(paramMap);
        return Result.ok();

    }



    //查询医院
    @PostMapping("hospital/show")
    public Result getHospital(HttpServletRequest request){
        //获取传过来的医院信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSign = (String)paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String hoscode = (String)paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);


        //3.把数据库中查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hospSign.equals(signKeyMd5)){
            throw  new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        //根据医院编号进行查询
        Hospital hospital = hospitalService.getByHoscode(hoscode);
        return Result.ok(hospital);
    }



    //上传医院接口
    @PostMapping("saveHospital")
    public Result saveHosp(HttpServletRequest request){
        //获取传过来的医院信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSign = (String)paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String hoscode = (String)paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);


        //3.把数据库中查询签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if(!hospSign.equals(signKeyMd5)){
            throw  new YyghException(ResultCodeEnum.SIGN_ERROR);
        }


        //传输过程中“+”转换为了“ ”，因此我们要转换回来
        String logoData = (String)paramMap.get("logoData");
        logoData = logoData.replaceAll(" ", "+");
        paramMap.put("logoData",logoData);

        //调用服务
        hospitalService.save(paramMap);
        return Result.ok();
    }
}
