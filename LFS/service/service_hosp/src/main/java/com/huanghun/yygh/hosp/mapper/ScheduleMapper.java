package com.huanghun.yygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanghun.yygh.model.hosp.Schedule;

public interface ScheduleMapper extends BaseMapper<Schedule> {
}
