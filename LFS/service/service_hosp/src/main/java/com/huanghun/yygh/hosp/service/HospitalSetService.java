package com.huanghun.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanghun.yygh.model.hosp.HospitalSet;
import com.huanghun.yygh.vo.order.SignInfoVo;

public interface HospitalSetService extends IService<HospitalSet> {
    String getSignKey(String hospcode);

    SignInfoVo getSignInfoVo(String hoscode);
}
