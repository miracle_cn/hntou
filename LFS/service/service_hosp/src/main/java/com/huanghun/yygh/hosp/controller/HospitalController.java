package com.huanghun.yygh.hosp.controller;

import com.huanghun.yygh.common.result.Result;
import com.huanghun.yygh.hosp.service.HospitalService;
import com.huanghun.yygh.vo.hosp.HospitalQueryVo;
import com.huanghun.yygh.model.hosp.Hospital;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/admin/hosp/hospital")
public class HospitalController {
    @Autowired
    private HospitalService hospitalService;

    @RequestMapping("list/{page}/{limit}")
    public Result lisyHosp(@PathVariable Integer page,
                           @PathVariable Integer limit,
                           HospitalQueryVo hospitalQueryVo){
        Page<Hospital> pages = hospitalService.selectHospPage(page,limit,hospitalQueryVo);
        return Result.ok(pages);
    }

    //根据id更新上线状态
    @ApiOperation(value = "更新状态")
    @GetMapping("updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable String id,
                               @PathVariable Integer status){
        hospitalService.updateStatus(id,status);
        return Result.ok();
    }

    //医院详情信息
    @ApiOperation(value = "医院详情信息")
    @GetMapping("showHospDetail/{id}")
    public Result showHospDetail(@PathVariable String id){
        Map<String, Object> map = hospitalService.getHospById(id);
        return Result.ok(map);
    }

}
