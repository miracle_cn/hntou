# Generated by Django 3.2.8 on 2021-12-07 12:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homework', '0006_teacher'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='stunu',
            new_name='teanu',
        ),
    ]
