# Generated by Django 3.2.8 on 2021-12-05 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homework', '0004_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='algorithm',
            field=models.IntegerField(blank=True, null=True, verbose_name='算法'),
        ),
        migrations.AddField(
            model_name='student',
            name='linux',
            field=models.IntegerField(blank=True, null=True, verbose_name='linux高级程序设计'),
        ),
        migrations.AddField(
            model_name='student',
            name='php',
            field=models.IntegerField(blank=True, null=True, verbose_name='php程序设计'),
        ),
        migrations.AddField(
            model_name='student',
            name='ruanjiangongcheng',
            field=models.IntegerField(blank=True, null=True, verbose_name='软件工程'),
        ),
        migrations.AddField(
            model_name='student',
            name='shujujiegou',
            field=models.IntegerField(blank=True, null=True, verbose_name='数据结构'),
        ),
        migrations.AddField(
            model_name='student',
            name='uml',
            field=models.IntegerField(blank=True, null=True, verbose_name='uml画图'),
        ),
    ]
