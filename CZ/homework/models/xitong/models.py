from django.db import models
from django.contrib.auth.models import User

class Student(models.Model):
    #user = models.OneToOneField(User,on_delete=models.CASCADE) 
    name = models.CharField(verbose_name='学生姓名',max_length=20)
    age = models.IntegerField(verbose_name='学生年龄',default=20)
    email = models.EmailField(verbose_name='学生邮箱',max_length=50,null=True,blank=True)
    cls = models.ForeignKey('Class',default=1,on_delete=models.CASCADE,verbose_name='班级')
    stunu = models.CharField(verbose_name='学号',max_length=10,null=True,blank=True)
    ruanjiangongcheng = models.IntegerField(verbose_name="软件工程",null=True,blank=True)
    shujujiegou = models.IntegerField(verbose_name="数据结构",null=True,blank=True)
    uml = models.IntegerField(verbose_name="uml画图",null=True,blank=True)
    linux = models.IntegerField(verbose_name="linux高级程序设计",null=True,blank=True)
    php = models.IntegerField(verbose_name="php程序设计",null=True,blank=True)
    algorithm = models.IntegerField(verbose_name="算法",null=True,blank=True)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '学生表'


class Class(models.Model):
    name = models.CharField(verbose_name='班级名',max_length=20)
    banzhuren = models.CharField(verbose_name='班主任',max_length=10)
    class Meta:
        verbose_name = verbose_name_plural = '班级表'
    def __str__(self):
        return self.name

class UserProfile(models.Model):
    nick = models.CharField(verbose_name='真实姓名',max_length=30)
    age = models.IntegerField(verbose_name='真实年龄')
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    number = models.CharField(verbose_name='工号',max_length=10,null=True,blank=True)
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    #grades = models.ManyToManyField('Grades')

    class Meta:
        verbose_name = verbose_name_plural = '用户表'

    def __str__(self):
        return self.nick

class Teacher(models.Model):
    name = models.CharField(verbose_name='教师姓名',max_length=20)
    age = models.IntegerField(verbose_name='教师年龄',default=30)
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    teanu = models.CharField(verbose_name='教师工号',max_length=10,null=True,blank=True)
    

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '教师表'

class Banzhuren(models.Model):
    name = models.CharField(verbose_name='教师姓名',max_length=20)
    age = models.IntegerField(verbose_name='教师年龄',default=30)
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    teanu = models.CharField(verbose_name='教师工号',max_length=10,null=True,blank=True)
    

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '班主任表'

class Mishu(models.Model):
    name = models.CharField(verbose_name='教师姓名',max_length=20)
    age = models.IntegerField(verbose_name='教师年龄',default=30)
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    teanu = models.CharField(verbose_name='教师工号',max_length=10,null=True,blank=True)
    

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '秘书表'

class Fudaoyuan(models.Model):
    name = models.CharField(verbose_name='教师姓名',max_length=20)
    age = models.IntegerField(verbose_name='教师年龄',default=30)
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    teanu = models.CharField(verbose_name='教师工号',max_length=10,null=True,blank=True)
    

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '辅导员表'

class Fuzeren(models.Model):
    name = models.CharField(verbose_name='教师姓名',max_length=20)
    age = models.IntegerField(verbose_name='教师年龄',default=30)
    email = models.EmailField(verbose_name='邮箱',max_length=50,null=True,blank=True)
    teanu = models.CharField(verbose_name='教师工号',max_length=10,null=True,blank=True)
    

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = verbose_name_plural = '专业负责人表'



###成绩的解决在于学生表和成绩表的连接



