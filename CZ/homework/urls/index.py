from django.urls import path, include
from homework.views.index import index, manage, config, base, stu_del, stu_edit, login, reg, creatuser, logout, enterxs, stuwatch, stuinfo, stugrade
from homework.views.index import teawatch, teainfo, teawatchgrade, teachangegrade, bzrwatchinfo, bzrchangeinfo, bzrwatchstuinfo, bzrchangestuinfo
from homework.views.index import bzrwatchstugrade, msaddstu, msmanagestu, mswatchinfo, msdelstu, mseditstu, mseditinfo
from homework.views.index import fzrwatchinfo, fzreditinfo, fzrchoosecls, fzrwatchgrade
from homework.views.index import fdywatchinfo, fdyeditinfo, fdywatchcls, fdyeditstu
app_name='common'
urlpatterns = [
    path("",base,name="base"),#这条路径自己加的

    #管理员路径

    path("index/", index, name="index"),
    path("manage/",manage,name="manage"),
    path("config/",config,name="config"),
    path("stu_del/",stu_del,name="stu_del"),
    path("stu_edit/",stu_edit,name="stu_edit"),

    #登录路径

    path("login/",login,name="login"),
    path("reg/",reg,name="reg"),
    path("creatuser/",creatuser,name="creatuser"),
    path("logout/",logout,name="logout"),

    #学生路径

    path("enterxs/",enterxs,name="enterxs"),
    path("stuwatch/",stuwatch,name="stuwatch"),
    path("stuinfo/",stuinfo,name="stuinfo"),
    path("stugrade/",stugrade,name="stugrade"),

    #老师路径

    path("teawatch/",teawatch,name="teawatch"),
    path("teainfo/",teainfo,name="teainfo"),
    path("teawatchgrade/",teawatchgrade,name="teawatchgrade"),
    path("teachangegrade/",teachangegrade,name="teachangegrade"),

    #班主任路径

    path("bzrwatchinfo/",bzrwatchinfo,name="bzrwatchinfo"),
    path("bzrchangeinfo/",bzrchangeinfo,name="bzrchangeinfo"),
    path("bzrwatchstuinfo/",bzrwatchstuinfo,name="bzrwatchstuinfo"),
    path("bzrchangestuinfo/",bzrchangestuinfo,name="bzrchangestuinfo"),
    path("bzrwatchstugrade/",bzrwatchstugrade,name="bzrwatchstugrade"),

    #秘书路径

    path("msaddstu/",msaddstu,name="msaddstu"),
    path("msdelstu/",msdelstu,name="msdelstu"),
    path("mseditstu/",mseditstu,name="mseditstu"),
    path("msmanagestu/",msmanagestu,name="msmanagestu"),
    path("mswatchinfo/",mswatchinfo,name="mswatchinfo"),
    path("mseditinfo/",mseditinfo,name="mseditinfo"),

    #专业负责人路径

    path("fzrwatchinfo/",fzrwatchinfo,name="fzrwatchinfo"),
    path("fzreditinfo/",fzreditinfo,name="fzreditinfo"),
    path("fzrchoosecls/",fzrchoosecls,name="fzrchoosecls"),
    path("fzrwatchgrade/",fzrwatchgrade,name="fzrwatchgrade"),

    #辅导员路径
    path("fdywatchinfo/",fdywatchinfo,name="fdywatchinfo"),
    path("fdyeditinfo/",fdyeditinfo,name="fdyeditinfo"),
    path("fdywatchcls/",fdywatchcls,name="fdywatchcls"),
    path("fdyeditstu/",fdyeditstu,name="fdyeditstu"),
    
    
    
    #path("banzhuren/", include("homework.urls.banzhuren.index")),
    #path("laoshi/", include("homework.urls.laoshi.index")),
    #path("mishu/", include("homework.urls.mishu.index")),
    #path("zhuanyefuzeren/", include("homework.urls.zhuanyefuzeren.index")),
    #path("admin/", include("homework.urls.admin.index")),
    #path("menu/", include("homework.urls.menu.index")),
    #path("xuesheng/", include("homework.urls.xuesheng.index")),
]

