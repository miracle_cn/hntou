from django.contrib import admin
from homework.models.xitong.models import Student, Class, UserProfile, Teacher, Banzhuren, Mishu
from homework.models.xitong.models import Fudaoyuan, Fuzeren
# Register your models here.

admin.site.register(Student)
admin.site.register(Class)
admin.site.register(UserProfile)
admin.site.register(Teacher)
admin.site.register(Banzhuren)
admin.site.register(Mishu)
admin.site.register(Fudaoyuan)
admin.site.register(Fuzeren)
