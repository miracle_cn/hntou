from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from homework.models.xitong import models
from django.contrib.auth.models import User
from django.contrib import auth
from django.urls import reverse
from django.contrib.auth.decorators import login_required

def base(request):#这个函数自己加的
    return render(request,"xitong/base.html")

@login_required
def index(request):
    
    if request.method=="POST":#添加操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        #email1 = request.POST.get('email',None)
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)

        '''stu_info = {
            'name' : name1,
            'age' : age1,
            'cls.name' : cid,
            'stunu' : stunu1,
            
        }'''
        
       
        try:
             s1 = models.Student()
             s1.name=name1
             s1.age=age1
             #s1.email=email1
             s1.cls_id=cid
             s1.stunu=stunu1
             s1.save()
             '''s = models.Student.objects.create(**stu_info)
             s.save()'''
            
             return HttpResponseRedirect('/xitong/manage/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        classes = models.Class.objects.all()
        context = {
            'index' : 'active',
            'classes' : classes,
        }
    return render(request, "xitong/index.html",context)

@login_required
def manage(request):
    keyword = request.GET.get('keyword',None)
    if keyword is not None:
        stus = models.Student.objects.filter(name__icontains=keyword).all()
    else:
        stus = models.Student.objects.all()
    context = {
        'manage' : 'active',
        'stus' : stus,
    }
    return render(request,"xitong/manage.html",context)


def config(request):
    context = {
        'config' : 'active'
    }
    return render(request,"xitong/config.html",context)


def stu_del(request):
    #获取学生id
    sid = request.GET.get('sid',None)
    if sid is not None:
        sid = int(sid)
        res = models.Student.objects.filter(id=sid).delete()#res的值为删除操作影响的行数
        if res != 0:
            return HttpResponseRedirect('/xitong/manage/')#返回到根
        else:
            return HttpResponse('删除失败，数据异常')
    else:
         return HttpResponse('数据异常')


def stu_edit(request):
    sid = request.GET.get('sid',None)
    stu = models.Student.objects.get(pk=sid) #获取修改学生id

    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        #email1 = request.POST.get('email',None)
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)
        
        
       
        try:
             '''stu = models.Student.objects.get(id=sid)
             stu.name=name1
             stu.age=age1
             stu.stunu=stunu1
             stu.cls_id=cid
             stu.save()'''
             
             models.Student.objects.filter(id=sid).update(
                 name=name1,
                 age=age1,
                 stunu=stunu1,
                 cls_id=cid,
             )

            
             
            
             return HttpResponseRedirect('/xitong/manage/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        
        
        classes = models.Class.objects.all()
        context = {
            'stu' : stu,
            'classes' : classes,
        }
    return render(request, "xitong/edit.html",context)


def login(request):
    identity = request.POST.get('identity',None) #111
    if request.method == "POST":
        error = 'input your info'
        username = request.POST.get('username',None)
        password = request.POST.get('password',None)
        if username and password:
            user = auth.authenticate(username=username,password=password)
            if user is not None:
                auth.login(request,user)
                
                if identity == "学生": #
                    return render(request,'identity/xuesheng.html')
                elif identity == "老师":#
                    return render(request,'teacher/laoshi.html')#
                elif identity == "班主任":
                    return render(request,"banzhuren/banzhuren.html")
                elif identity == "秘书":
                    return render(request,"mishu/mishu.html")
                elif identity == "专业负责人":
                    return render(request,"fuzeren/fuzeren.html")
                elif identity == "辅导员":
                    return render(request,'fudaoyuan/fudaoyuan.html')
                else:#
                    return render(request,'xitong/index.html')

            else:
                error = '用户名或密码错误'
                return render(request,'login/login.html',{'error' : error})
        else:
            error = '用户名或者密码不能为空'
            return render(request,'login/login.html',{'error' : error})

    elif request.method == "GET":
        
        return render(request,"login/login.html")

def reg(request):
    username = request.POST.get('username',None)
    print(username)
    password1 = request.POST.get('password1','123456')
    print(password1)
    password2 = request.POST.get('password2','123456')
    print(password2)
    nick = request.POST.get('nick','匿名')
    print(nick)
    age = request.POST.get('age','')
    print(age)
    number = request.POST.get('number','')
    print(number)

    
                 
    if username and password1 and password2:
        if password1 == password2:
            #uc = User.objects.filter(username=username).all().count() #验证用户名是否存在
            
            #if uc == 0: #用户名不存在
            if User.objects.filter(username=username).exists():
                error = '用户名已经被注册'
                return render(request,'login/reg.html',{'error':error})
                 
            else:
                 user = User.objects.create_user(username=username,password=password1)
                 #user = User.objects.create_user(username=username)
                 user.set_password(password1)
                 userprofile = models.UserProfile()
                 userprofile.nick = nick
                 userprofile.age = age
                 userprofile.user = user
                 userprofile.number = number
                 userprofile.save()
                 
                 #注册成功，跳转页面
                 return render(request,'login/login.html')
            
        else:
            error = '两次密码不相同'
            return render(request,'login/reg.html',{'error':error})
    else:
        error = '用户名密码不能为空'
        return render(request,'login/reg.html',{'error':error})


def creatuser(request):
    user = User.objects.create_user(username='chengzi',password='123456')
    userprofile = models.UserProfile()
    userprofile.nick = '橙子'
    userprofile.age = 20
    userprofile.user = user
    userprofile.number = '12345678'
    userprofile.save()
    return HttpResponse('ok')

def logout(request):
    auth.logout(request)
    return render(request,'login/login.html')




####下面是对学生界面的操作
def enterxs(request):
    return render(request,'identity/xuesheng.html')

#查看学生信息

@login_required
def stuwatch(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    stu = models.Student.objects.get(stunu=number)
    print(stu.uml)
    print(stu.linux)
    print(user)
    
        
        

    context = {
            'index' : 'active',
            'stu' : stu,
            
    }
    return render(request, "identity/watch.html",context)

#修改学生信息

@login_required
def stuinfo(request):

    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    #return render(request,'identity/changeinfo.html')
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        #email1 = request.POST.get('email',None)
        
        cid = request.POST.get('cid',None)


        
        #stu = models.Student.objects.get(stunu=number)

        models.Student.objects.filter(stunu=number).update(
                 name=name1,
                 age=age1,
                 
                 cls_id=cid,
             )
        return HttpResponseRedirect('/xitong/stuwatch/')
        

    
    elif request.method=="GET":
        
        stu = models.Student.objects.get(stunu=number)
        print(stu)
        print(stu.stunu)
        classes = models.Class.objects.all()
        context = {
            'stu' : stu,
            'classes' : classes,
            'manage' : 'active',
    }
    return render(request, "identity/changeinfo.html",context)

#查看学生成绩

def stugrade(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    stu = models.Student.objects.get(stunu=number)
    context = {
        'stu' : stu,
        'grade' : 'active',
    }
    return render(request,"identity/grade.html",context)




#下面是对于老师的操作

#查看老师信息
def teawatch(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    tea = models.Teacher.objects.get(teanu=number)
    
    context = {
            'index' : 'active',
            'tea' : tea,
            
    }
    return render(request, "teacher/watch.html",context)
#修改老师信息
def teainfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        email1 = request.POST.get('email',None)

        models.Teacher.objects.filter(teanu=number).update(
                 name=name1,
                 age=age1,
                 email=email1,
             )
        return HttpResponseRedirect('/xitong/teawatch/')
        

    
    elif request.method=="GET":
        
        tea = models.Teacher.objects.get(teanu=number)
        context = {
            'tea' : tea,
            'manage' : 'active',
    }
    return render(request, "teacher/changeinfo.html",context)

#老师查看成绩

def teawatchgrade(request):
    keyword = request.GET.get('keyword','')
    order = request.GET.get('order',None)
    rule = request.GET.get('rule',None)
    if keyword is not None:
        stus = models.Student.objects.filter(name__icontains=keyword).all()
    else:
        stus = models.Student.objects.all()

    if order is not None:
        if rule == 'up':
            stus = stus.order_by(order)
        else:
            stus = stus.order_by('-' + order)
    
    context = {
        'watchgrade' : 'active',
        'stus' : stus,
        'keyword' : keyword,
    }
    return render(request,"teacher/watchgrade.html",context)

#老师修改成绩

def teachangegrade(request):
    sid = request.GET.get('sid',None)
    
    stu = models.Student.objects.get(pk=sid) #获取修改学生id

    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        
        uml = request.POST.get('umlgrade',None)
        
        
       
        try:
             
             
             models.Student.objects.filter(id=sid).update(
                 name = name1,
                 uml = uml,
             )

            
             
             return HttpResponseRedirect('/xitong/teawatchgrade/')
             #return render(request,'teacher/watchgrade.html')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        
        
        
        context = {
            'stu' : stu,
            'editgrade' : 'active',
        }
    return render(request, "teacher/editgrade.html",context)



#下面是对班主任的操作

#查看个人信息
def bzrwatchinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    bzr = models.Banzhuren.objects.get(teanu=number)
    
    context = {
            'index' : 'active',
            'bzr' : bzr,
            
    }
    return render(request, "banzhuren/watch.html",context)

#修改个人信息
def bzrchangeinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        email1 = request.POST.get('email',None)

        models.Banzhuren.objects.filter(teanu=number).update(
                 name=name1,
                 age=age1,
                 email=email1,
             )
        return HttpResponseRedirect('/xitong/bzrwatchinfo/')
        

    
    elif request.method=="GET":
        
        bzr = models.Banzhuren.objects.get(teanu=number)
        context = {
            'bzr' : bzr,
            'manage' : 'active',
    }
    return render(request, "banzhuren/changeinfo.html",context)

#查看所有学生信息
def bzrwatchstuinfo(request):
    keyword = request.GET.get('keyword',None)
    if keyword is not None:
        stus = models.Student.objects.filter(name__icontains=keyword).all()
    else:
        stus = models.Student.objects.all()
    context = {
        'watchstuinfo' : 'active',
        'stus' : stus,
    }
    return render(request,"banzhuren/watchstuinfo.html",context)


def config(request):
    context = {
        'config' : 'active'
    }
    return render(request,"xitong/config.html",context)

#修改学生信息
def bzrchangestuinfo(request):
    sid = request.GET.get('sid',None)
    stu = models.Student.objects.get(pk=sid) #获取修改学生id

    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        #email1 = request.POST.get('email',None)
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)
        
        
       
        try:
             models.Student.objects.filter(id=sid).update(
                 name=name1,
                 age=age1,
                 stunu=stunu1,
                 cls_id=cid,
             )

            
             
            
             return HttpResponseRedirect('/xitong/bzrwatchstuinfo/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        
        
        classes = models.Class.objects.all()
        context = {
            'stu' : stu,
            'classes' : classes,
        }
    return render(request, "banzhuren/changestuinfo.html",context)
    

#班主任查看学生成绩
def bzrwatchstugrade(request):
    
    keyword = request.GET.get('keyword','')
    order = request.GET.get('order',None)
    rule = request.GET.get('rule',None)
    if keyword is not None:
        stus = models.Student.objects.filter(name__icontains=keyword).all()
    else:
        stus = models.Student.objects.all()
        

    if order is not None:
        if rule == 'up':
            stus = stus.order_by(order)
        else:
            stus = stus.order_by('-' + order)
    
    context = {
        'bzrwatchstugrade' : 'active',
        'stus' : stus,
        'keyword' : keyword,
    }
    print(stus[0].cls.name)
    return render(request,"banzhuren/watchstugrade.html",context)

#针对秘书的操作
#秘书增加学生
def msaddstu(request):
    if request.method=="POST":#添加操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        #email1 = request.POST.get('email',None)
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)

        try:
             s1 = models.Student()
             s1.name=name1
             s1.age=age1
             #s1.email=email1
             s1.cls_id=cid
             s1.stunu=stunu1
             s1.save()
        
             return HttpResponseRedirect('/xitong/msmanagestu/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        classes = models.Class.objects.all()
        context = {
            'index' : 'active',
            'classes' : classes,
        }
    return render(request, "mishu/addstu.html",context)


#秘书管理学生

def msmanagestu(request):
    keyword = request.GET.get('keyword',None)
    if keyword is not None:
        stus = models.Student.objects.filter(name__icontains=keyword).all()
    else:
        stus = models.Student.objects.all()
    context = {
        'manage' : 'active',
        'stus' : stus,
    }
    return render(request,"mishu/managestu.html",context)

#秘书删除学生
def msdelstu(request):
    sid = request.GET.get('sid',None)
    if sid is not None:
        sid = int(sid)
        res = models.Student.objects.filter(id=sid).delete()#res的值为删除操作影响的行数
        if res != 0:
            return HttpResponseRedirect('/xitong/msmanagestu/')#返回到根
        else:
            return HttpResponse('删除失败，数据异常')
    else:
         return HttpResponse('数据异常')

#秘书修改学生
def mseditstu(request):
    sid = request.GET.get('sid',None)
    stu = models.Student.objects.get(pk=sid) #获取修改学生id

    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)
        
        
       
        try:
             
             
             models.Student.objects.filter(id=sid).update(
                 name=name1,
                 age=age1,
                 stunu=stunu1,
                 cls_id=cid,
             )

            
             
            
             return HttpResponseRedirect('/xitong/msmanagestu/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        
        classes = models.Class.objects.all()
        context = {
            'stu' : stu,
            'classes' : classes,
        }
    return render(request, "mishu/edit.html",context)

#秘书查看个人信息
def mswatchinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    ms = models.Mishu.objects.get(teanu=number)
    
    context = {
            'watchinfo' : 'active',
            'ms' : ms,
            
    }
    return render(request, "mishu/watchinfo.html",context)
    
#秘书修改个人信息
def mseditinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        email1 = request.POST.get('email',None)

        models.Mishu.objects.filter(teanu=number).update(
                 name=name1,
                 age=age1,
                 email=email1,
             )
        return HttpResponseRedirect('/xitong/mswatchinfo/')
        

    
    elif request.method=="GET":
        
        ms = models.Mishu.objects.get(teanu=number)
        context = {
            'ms' : ms,
            'editinfo' : 'active',
    }
    return render(request, "mishu/editinfo.html",context)


#以下是对于负责人的一些操作

#负责人查看自己的信息
def fzrwatchinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    fzr = models.Fuzeren.objects.get(teanu=number)
    
    context = {
            'watchinfo' : 'active',
            'fzr' : fzr,
            
    }
    return render(request, "fuzeren/watch.html",context)

#负责人修改自己的信息
def fzreditinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        email1 = request.POST.get('email',None)

        models.Fuzeren.objects.filter(teanu=number).update(
                 name=name1,
                 age=age1,
                 email=email1,
             )
        return HttpResponseRedirect('/xitong/fzrwatchinfo/')
        

    
    elif request.method=="GET":
        
        fzr = models.Fuzeren.objects.get(teanu=number)
        context = {
            'fzr' : fzr,
            'editinfo' : 'active',
    }
    return render(request, "fuzeren/changeinfo.html",context)

#负责人选择班级

def fzrchoosecls(request):
    if request.method=="POST":#添加操作
        
        cid = request.POST.get('cid',None)
        stus = models.Student.objects.all()
        cls = models.Class.objects.get(pk=cid)
        clsname = cls.name
        
        context = {
            'clsname' : clsname,
            'stus' : stus,
            'watchclsinfo' : 'active',
        }
        return render(request,'fuzeren/showclsinfo.html',context)
        
    elif request.method=="GET":
        classes = models.Class.objects.all()
        context = {
            'watchclsinfo' : 'active',
            'classes' : classes,
        }
    return render(request, "fuzeren/watchclassinfo.html",context)

#负责人查看班级信息

def fzrwatchgrade(request):
    if request.method=="POST":#添加操作
        
        cid = request.POST.get('cid',None)
        stus = models.Student.objects.all()
        cls = models.Class.objects.get(pk=cid)
        clsname = cls.name
        
        context = {
            'clsname' : clsname,
            'stus' : stus,
            'watchgrade' : 'active',
        }
        return render(request,'fuzeren/watchgrade.html',context)
        
    elif request.method=="GET":
        classes = models.Class.objects.all()
        context = {
            'watchgrade' : 'active',
            'classes' : classes,
        }
    return render(request, "fuzeren/watchclassinfo.html",context)

#以下是对辅导员做的操作

#辅导员查看个人信息
def fdywatchinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number
    fdy = models.Fudaoyuan.objects.get(teanu=number)
    
    context = {
            'watchinfo' : 'active',
            'fdy' : fdy,
            
    }
    return render(request, "fudaoyuan/watch.html",context)

#辅导员修改个人信息
def fdyeditinfo(request):
    user = request.user
    userprofile = models.UserProfile.objects.filter(user=user)
    number = userprofile[0].number

    
    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        email1 = request.POST.get('email',None)

        models.Fudaoyuan.objects.filter(teanu=number).update(
                 name=name1,
                 age=age1,
                 email=email1,
             )
        return HttpResponseRedirect('/xitong/fdywatchinfo/')
        

    
    elif request.method=="GET":
        
        fdy = models.Fudaoyuan.objects.get(teanu=number)
        context = {
            'fdy' : fdy,
            'editinfo' : 'active',
    }
    return render(request, "fudaoyuan/changeinfo.html",context)

#辅导员查看班级信息
def fdywatchcls(request):
    if request.method=="POST":#添加操作
        
        cid = request.POST.get('cid',None)
        stus = models.Student.objects.all()
        cls = models.Class.objects.get(pk=cid)
        clsname = cls.name
        
        context = {
            'clsname' : clsname,
            'stus' : stus,
            'choosecls' : 'active',
        }
        return render(request,'fudaoyuan/watchstuinfo.html',context)
        
    elif request.method=="GET":
        classes = models.Class.objects.all()
        context = {
            'choosecls' : 'active',
            'classes' : classes,
        }
    return render(request, "fudaoyuan/choosecls.html",context)



#辅导员修改学生信息
def fdyeditstu(request):
    sid = request.GET.get('sid',None)
    stu = models.Student.objects.get(pk=sid) #获取修改学生id

    if request.method=="POST":#修改操作
        name1 = request.POST.get('name',None)
        age1 = request.POST.get('age',None)
        stunu1 = request.POST.get('stunu',None)
        cid = request.POST.get('cid',None)
        
        
       
        try:
             models.Student.objects.filter(id=sid).update(
                 name=name1,
                 age=age1,
                 stunu=stunu1,
                 cls_id=cid,
             )

            
             
            
             return HttpResponseRedirect('/xitong/fdywatchcls/')
        except Exception:
             return HttpResponse('数据异常，添加失败！')
        
    elif request.method=="GET":
        
        
        classes = models.Class.objects.all()
        context = {
            'stu' : stu,
            'classes' : classes,
        }
    return render(request, "fudaoyuan/changestuinfo.html",context)
    




    

        
        



    

    


