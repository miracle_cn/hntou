create table PJ_goods(
    gid number(8) primary key not null,
    title varchar2(10),
    publish_time date,
    content varchar2(4000),
    buy_price float(2),
    sell_price float(2),
    picture varchar2(128),
    views number(10),
    status number(1),
    cid number(8),
    uuid number(8)
);
create table PJ_user(
    uuid number(8)primary key not null,
    username varchar2(16),
    userpwd varchar2(16),
    name varchar2(10),
    sex varchar2(2) constraint user_check_sex check(sex in('��', 'Ů')),
    academy varchar2(10),
    grade varchar2(10),
    qq varchar2(10),
    phone varchar2(11),
    profile varchar2(128)
);
insert into PJ_user values(10000, 'jct', 'jct', 'PJ', '��', '���ѧԺ', '2018��', '52051314', '18589588572', 'jct.jpg');
insert into PJ_user values(10001, '1', '1', 'PJ', '��', '���ѧԺ', '2018��', '52051314', '18589588572', 'jct1.jpg');

select * from  PJ_user;

create table PJ_admin(
    aid number(8) primary key not null,
    adminname varchar2(16),
    adminpwd varchar2(16),
    name varchar2(10),
    phone number(11)
);
create table PJ_message(
    mid number(8)primary key not null,
    content varchar2(4000),
    goods_id number(8)
);
create table PJ_category(
    cid number(8)primary key not null,
    name varchar2(10)
);
create table PJ_collection(
    cid number(8)primary key not null,
    gid number(8),
    uuid number(8)
);
create table PJ_wanted(
    id number(8) primary key not null,
    content varchar2(4000),
    uuid number(8),
    cid number(8)
);



-- PJadmin
create sequence PJ_admin_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_admin_seq
before insert on PJ_ADMIN for each row
begin 
	select PJ_admin_seq.nextval into :new.aid from dual;
end;

-- PJ_user
create sequence PJ_user_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_user_seq
before insert on PJ_USER for each row
begin 
	select PJ_user_seq.nextval into :new.uuid from dual;
end;

-- PJ_category
create sequence PJ_category_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_category_seq
before insert on PJ_category for each row
begin 
	select PJ_category_seq.nextval into :new.cid from dual;
end;

-- PJ_collection
create sequence PJ_collection_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_collection_seq
before insert on PJ_collection for each row
begin 
	select PJ_collection_seq.nextval into :new.cid from dual;
end;


-- PJ_goods
create sequence PJ_goods_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_goods_seq
before insert on PJ_goods for each row
begin 
	select PJ_goods_seq.nextval into :new.gid from dual;
end;

-- PJ_message
create sequence PJ_message_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_message_seq
before insert on PJ_message for each row
begin 
	select PJ_message_seq.nextval into :new.mid from dual;
end;

-- PJ_wanted
create sequence PJ_wanted_seq
increment by 1 
start with 1 
nomaxvalue 
nominvalue 
nocache;


create or replace trigger PJ_wanted_seq
before insert on PJ_wanted for each row
begin 
	select PJ_wanted_seq.nextval into :new.wid from dual;
end;



insert into PJ_admin(adminname, adminpwd, name, phone) values('jct1', 'jct', 'PJ','18589588572');
insert into PJ_admin(adminname, adminpwd, name, phone) values('jct2', 'jct', 'PJ','18589588572');

SELECT * from PJ_ADMIN

alter table PJ_admin add constraint adminname_uniq unique (adminname);
alter table PJ_user add constraint username_uniq unique (username);






select * from PJ_GOODS;

