package com.jwl.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jwl.constant.MessageConstant;
import com.jwl.entity.PageResult;
import com.jwl.entity.QueryPageBean;
import com.jwl.entity.Result;
import com.jwl.pojo.CheckItem;
import com.jwl.service.CheckItemService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/checkitem")
public class CheckItemController {

    @Reference//去服务中心查找服务
    private CheckItemService checkItemService;
    //新增检查项
    @RequestMapping("/add")
    public Result add(@RequestBody CheckItem checkItem){

        try{
            checkItemService.add(checkItem);

        }catch (Exception e){

            //调用失败就会进入catch
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
        }
        return new Result(true,MessageConstant.ADD_CHECKITEM_SUCCESS);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = checkItemService.pageQuery(queryPageBean);
        return pageResult;
    }

    @RequestMapping("/delete")
    public Result delete(Integer id){

        try{
            checkItemService.deleteById(id);

        }catch (Exception e){

            //调用失败就会进入catch
            return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_CHECKITEM_SUCCESS);
    }


    @RequestMapping("/edit")
    public Result edit(@RequestBody CheckItem checkItem){

        try{
            checkItemService.edit(checkItem);

        }catch (Exception e){

            //调用失败就会进入catch
            return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }


    @RequestMapping("/findById")
    public Result findById(Integer id){

        try{
            CheckItem checkItem = checkItemService.findById(id);
            return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
        }catch (Exception e){

            //调用失败就会进入catch
            return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);
        }

    }
}
