package com.jwl.service;

import com.jwl.entity.PageResult;
import com.jwl.entity.QueryPageBean;
import com.jwl.entity.Result;
import com.jwl.pojo.CheckItem;

public interface CheckItemService {
    public void add(CheckItem checkItem);

    public PageResult pageQuery(QueryPageBean queryPageBean);

    public void deleteById(Integer id);

    public void edit(CheckItem checkItem);

    public CheckItem findById(Integer id);

}
